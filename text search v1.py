''' The following code represents my first attempt at creating an enterprise search algorithm with a minimum reliance on the nltk package. This algorithm will take a dataframe of raw textual records 
and classify each record according to a pre-defined classification system. This is achieved by vectorizing each element of the dataframe and comparing it against each element of a dimension table
containing the already established classifiers using a cosine similarity scoring system. The classifier with the highest similarity score is then assigned to the record. 

If you are going to replicate this algorithm, please acknowledge me accordingly.
'''

# Importing the necessary python packages
import nltk
import os
import re
import string
import time
import pandas as pd 
from collections import Counter
from datetime import date, timedelta, datetime
from nltk.corpus import stopwords
from pyspark.sql import SQLContext, HiveCOntext, SparkSession, Row
from pyspark.sql.types import StructType, StructField, ArrayType, StringType, MapType
from pyspark import SparkContext, SparkConf
from pyspark.sql.functions import udf, input_file_name, col, expr, when, lit

nltk.download('stopwords')
stop_words = set([re.sub('[^a-zA-Z0-9 \n\.]', '', word) for word in stopwords.words("english")])
special_list = [char for char in string.punctuation]


# Defining the necessary methods for this script
def textCleanser(x):
	if x == None:
		return ""
	x1 = x.lower()
	x1 = x1.replace("\r\n", "\ n")
	x1 = x1.replace("/n", "/ n")
	x1 = x1.replace("\n", "\ n")
	x1 = x1.replace("/t", "/ t")
	x1 = x1.replace("\t", "\ t")
	x1 = x1.replace("/r", "/ r")
	x1 = x1.replace("\r", "\ r")
	for i in special_list:
		x1 = x1.replace(str(i), " ")
	x1 = re.sub('\s+', ' ', x1).strip()
	return x1

def textCounter(x):
	x1 = [i for i in textCleanser(x).split(" ") if i not in stop_words]
	if len(x1) == 0:
		return ""
	x1_final = Counter(x1)
	return x1_final

def textSet(x):
	x1 = [i for i in set(textCleanser(x).split(" ")) if i not in stop_words]
	if len(x1) == 0:
		return ""
	return set(x1)

def cosine(x,y):
	combined_view = list(set(x["reason_set"]).intersection(y["reason_set"]))
	if combined_view == []:
		return 0 
	numerator, denomin_1, denomin_2 = 0,0,0
	combined_view = list(set(x["reason_set"]).union(y["reason_set"]))
	x_range = [x["reason_counter"][i] if i in x["reason_counter"] else 0 for i in combined_view]
	y_range = [y["reason_counter"][i] if i in y["reason_counter"] else 0 for i in combined_view]
	numerator = sum([a*b for a,b in zip(x_range, y_range)])
	denomin_1 = sum(map(lambda x: x**2, x_range))
	denomin_2 = sum(map(lambda x: y**2, y_range))
	return (numerator / ((denomin_1 **0.5) * (denomin_2 ** 0.5)))

def fullMatcher(x,ylist):
	rankin = None,0
	for i in range(0,len(ylist),1):
		entry_score = cosine(x,ylist[i])
		if entry_score > rankin[1]:
			rankin = ylist[i]["reason_code"], entry_score
	return rankin

# Importing the dimension table and converting it into a dictionary format.
compare_df = pd.read_csv("insert table name here")
compare_df.drop_duplicates()
compare_df = compare_df.rename("reason_code", "reason_description")
compare_df["reason_counter"] = compare_df["reason_description"].apply(lambda x: textCounter(x))
compare_df["reason_set"] = compare_df["reason_description"].apply(lambda x: textSet(x))
compareList = compare_df.to_dict("records")
del compare_df

# Importing the dataframe of text that will need to be classified.

filler_df = (spark.sql(filler_query)).toPandas()
filler_df["reason"] = filler_df["reason"].apply(lambda x: textCleanser(x)) 
filler_df = filler_df.drop_duplicates()
filler_df["reason_counter"] = filler_df["reason"].apply(lambda x: textCounter(x)) 
filler_df["reason_set"] = filler_df["reason"].apply(lambda x: textSet(x)) 
filler_df[["reason_code", "reason_score"]] = filler_df.apply(lambda x:pd.Series(fullMatcher(x, compareList)), axis=1) 