# Uses Python 3
## Written as part of a problem set for the Data Structures & Algorithm course hosted in Cousera: https://www.coursera.org/specializations/data-structures-algorithms
import sys

def get_optimal_value(capacity, weights, values):
    sack_weight = 0 
    sack_value = 0
    edited_weights = weights
    edited_values= values	
    weighted_values = [edited_values[i]/edited_weights[i] for i in range(0,len(weights),1)]
    
    while sack_weight < capacity and len(weighted_values) > 0:

        largest = weighted_values.index(max(weighted_values))
        
        if edited_weights[largest] <= (capacity - sack_weight):
            sack_weight += edited_weights[largest]
            sack_value += edited_values[largest]
            del weighted_values[largest]
            del edited_weights[largest]
            del edited_values[largest]
        else: 
            sack_value += ((capacity - sack_weight) / edited_weights[largest]) * edited_values[largest]
            sack_weight = capacity
 #       print("sack weight is: "+ sack_weight)
 #       print("sack value is: "+ sack_value)

    return sack_value 


if __name__ == "__main__":
    data = list(map(int, sys.stdin.read().split()))
    n, capacity = data[0:2]
    values = data[2:(2 * n + 2):2]
    weights = data[3:(2 * n + 2):2]
    opt_value = get_optimal_value(capacity, weights, values)
    print("{:.10f}".format(opt_value))
