


def euclid_calculate(new_user):
	# setting up the input into its proper format. Seel line 41 and 42 for a desscription of what each index means,
	new_det_val = [0, 1, new[2], 0, new[3], new[4], new[5], new[6], new[7], new[8], new[9]]

	## Methods to Apply Within Functions ##
	# Method to compare individuals
	def cosine(x,y):
		if x==[] and y==[]:
			return 0
		numerator = 0 
		denomin_1 = 0
		denomin_2 = 0
		for i in range(0, len(x), 1):
			numerator += x[i] * y[i]
			denomin_1 += x[i] ** 2
			denomin_2 += y[i] ** 2
		return numerator / ((denomin_1 ** 0.5) * (denomin_2 ** 0.5))

	# Method to compare the ages of the new user and other existing users.
	def age_comp(x,y):
		return(abs((y - x)/x))

	# Method to compare the relationship status of the new user and other existing users.
	def relation_comp(x,y):
		if x == y:
			return 1
		elif sorted((x,y)) == ['married', 'in a relationship']:
			return 0.5
		elif sorted((x,y)) == ['in a relationship', 'single']:
			return 0.25
		else:
			return 0

	# Industry comparison of new users and existing users
	def industry_comp(x,y):
		if x.lower()==y.lower():
			return 1
		else:
			return 0

	# Defining the comparison list. The list stores matching data in the following order: 1) Age, 2) Maritial Status, 3) BLue Collar, 4) Industry, 5) Financial Stress, 
	# 6) Family Stress, 7) Relational Stress, 8) Working Stress, 9) Chronic Stress, 10) Other Stress
	matchin_list = { 'AdamIsAwesome': [35,'married',1,'construction',1,1,1,1,0,0],
					'JohnnyB':[22,'married',0,'finance',0,0,0,1,0,1],
					'StressH3ad':[24,'married',0,'marketing',1,0,1,1,0,1],
					'JasonIsAwesome':[47,'in a relationship',0,'technology'0,0,0,1,0,0],
					'AkashIsAwesome':[27,'in a relationship',1,'law'1,1,0,1,1,1],
					'BrianIsAwesome':[63,'single'1,'marketing',1,'unemployed',1,1,0,0,0,1]}
	
	# Initialising an empty list to sture the similarity scores
	rankin_dict = []

	# Populating scores for each entry in the comparison list and storing these scores in the ranking list
	for entry in matchin_list:
		matchin_list[0] = age_comp(new_user[0], matchin_list[entry][0])
		matchin_list[1] = relation_comp(new_user[1], matchin_list[entry][1])
		matchin_list[3] = industry_comp(new_user[3], matchin_list[entry][3])
		sim_score = cosine(new_det_val, matchin_list[entry])
		rankin_dict.append([entry, sim_score])

	rankin_dict1 = sorted(rankin_dict, key = lambda x: x[1], reverse = True)

	# Returning the top 5 peers
	return ranking_dict[0:5]