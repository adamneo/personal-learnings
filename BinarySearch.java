'''
The below code is my attempts at building the binary search algorithm from scratch.
'''
class BinarySearch {

	static boolean found = false; 
	static int counter = 0; 

	// The following method was developed to handle the scenario where it is likely that the key does not exist within the array. The method takes in an array of size 2 and assesses whether any of the values
	// are equal to the key. If this does not occur, then the method returns the lower value if the key is less than or equal to the lower bound, the upper bound if the key is greater than the upper bound or
	// the last index in all other scenarios.
	static int binarray2 (int key, int [] x, int i , int j) {
		if (x[i] >= key) {
			if (x[i] == key) {
				found = true; 
			}
			return i;
		}
		else if (x[j-1] >= key) {
			if (x[j-1] == key) {
				found = true;
			}
			return j-1;
		}
		else {
			return j;
		}
	}

	// The following runs the actual binary search algorithm.
	static int binSearch(int key, int [] x, int i, int j) {
		// Handling all scenarios where a split comprises of more than 2 numbers.
		if ((j-i) > 2) {
			int mid_point = (int) (i+j)/2;
			if (x[mid_point] == key && x[mid_point] != x[mid_point - 1]){
				found = true; 
				return mid_point; 
			}
			else if (x[mid_point] >= key) { 
				counter += 1; 
				return binSearch(key, x, i, mid_point); 	
			}
			else {
				counter += 1; 
				return binSearch(key,x,mid_point,j);
			}
		}
		// Handling the scenarios where the key does not exist within the x array.
		else {
			return binarray2(key,x,i,j);
		}
	}

	// Method to insert a value into the array should the key not exist originally.
	static int [] binInsert(int key, int [] x) {
		int search_loc = binSearch(key,x,0,x.length);
		int [] new_array = new int [(x.length+1)]; 
		if (found == true) {
			return x; 
		}
		else {
			if (search_loc == 0) {
				new_array[0] = key; 
				for ( int i=0; i < x.length; i++ ) {
					new_array[i+1] = x[i];
				}
			}
			else if (search_loc == x.length) {
				for ( int i=0; i < x.length; i++){
					new_array[i] = x[i]; 
				}
				new_array[x.length] = key; 
			}
			else {
				for ( int i=0; i<search_loc; i++) {
					new_array[i] = x[i];
				}
				new_array[search_loc] = key; 
				for ( int i=search_loc; i<x.length; i++) {
					new_array[i+1] = x[i];
				}
			}
		}
		return new_array; 
	}


	public static void main (String[] args) {
		// Initial tests for Exercises 3.2 and 3.3
		//int [] test = new int [6]; 
		//test[0] = 2;
		//test[1] = 4;
		//test[2] = 4;
		//test[3] = 4;
		//test[4] = 8;
		//test[5] = 9;

		//System.out.println(binSearch(8,test,0,6)); 
		//System.out.println(binSearch(4,test,0,6)); 
		//System.out.println(binSearch(8,test,3,5));
		//System.out.println(binSearch(2,test,0,6));
		//System.out.println(binSearch(9,test,0,6));
		//System.out.println(binSearch(7,test,0,6));
		//System.out.println(binSearch(11,test,4,6));

		// Tests for 3.4 - Setting up the arrays
		int [] array1 = {1,3,6,7,7,7,9}; 

		// Tests for 3.4 - Performing the tests
		System.out.print("Testing a small array: "); 
		for (int i=0; i<array1.length; i++){
			System.out.print (array1[i] + " "); 
		}
		System.out.println(); 
		int [][] test_cases1 = {{1,0,6},{9,0,6},{7,0,6},{11,0,6},{0,0,6},{8,0,6},{3,0,4}}; 
		for (int a=0; a<test_cases1.length; a++) {
			found = false; 
			counter = 0; 
			int key = test_cases1[a][0];
			int [] x_1 = array1.clone();
			int i_1 = test_cases1[a][1];
			int j_1 = test_cases1[a][2];
			int index = binSearch(key, x_1, i_1,j_1); 
			if (found == true) {
				System.out.println("Key " + key + " found at index " + index + " after " + counter + " binary search iterations."); 
			} else {
				System.out.println("Key " + key + " was not found. The key should be at index " + index + " after " + counter + " binary search iterations.");
			}
		}

		System.out.println(); 
		System.out.println(); 

		int [] array2 = new int [(int) Math.pow(10,7)];
		for (int i=0; i<array2.length; i++){
			array2[i] = 2 * i;
		}
		System.out.println("Testing a large  array: 0  2  4  6  8  ....  19999998"); 
		int [][] test_cases2 = {{5000000,0,array2.length},{8088008,0,array2.length},{5000001,0,array2.length},{39999998,0,array2.length},{0,0,array2.length}}; 
		for (int a=0; a<test_cases2.length; a++) {
			found = false; 
			counter = 0; 
			int key_2 = test_cases2[a][0];
			int [] x_2 = array2.clone();
			int i_2 = test_cases2[a][1];
			int j_2 = test_cases2[a][2];
			int index_2 = binSearch(key_2, x_2, i_2,j_2); 
			if (found == true) {
				System.out.println("Key " + key_2 + " found at index " + index_2 + " after " + counter + " binary search iterations."); 
			} else {
				System.out.println("Key " + key_2 + " was not found. The key should be at index " + index_2 + " after " + counter + " binary search iterations.");
			}		
		}

		System.out.println(); 
		System.out.println(); 

		int [] array3 = new int [(int) Math.pow(10,7)];
		for (int j=0; j<array3.length; j++){
			array3[j] = 10;
		}
		System.out.println("Testing a large  array: 10 10  10  ....  10"); 
		int [][] test_cases3 = {{10,0,array2.length},{10,346,array2.length},{11,0,array2.length},{9,0,array2.length}}; 
		for (int a=0; a<test_cases3.length; a++) {
			found = false; 
			counter = 0; 
			int key_3 = test_cases3[a][0];
			int [] x_3 = array3.clone();
			int i_3 = test_cases3[a][1];
			int j_3 = test_cases3[a][2];
			int index_3 = binSearch(key_3, x_3, i_3, j_3); 
			if (found == true) {
				System.out.println("Key " + key_3 + " found at index " + index_3 + " after " + counter + " binary search iterations."); 
			} else {
				System.out.println("Key " + key_3 + " was not found. The key should be at index " + index_3 + " after " + counter + " binary search iterations.");
			}		
		}
	System.out.println();
	System.out.println();
	System.out.println();
	// Testing out the array insert function
	int [] test_insert_array = new int[] {5,5,5,6,6,10,10,10,11};
	System.out.print("Now testing out our insert function. The original array appears as follows : "); 
	for (int i=0; i<test_insert_array.length; i++) {
		System.out.print (test_insert_array[i] + " "); 
	}
	System.out.println(); 
	int [] test_insert_array2 = binInsert(8, test_insert_array);
		System.out.print("Viewing the ammended array. The array now appears as follows : "); 
	for (int i=0; i<test_insert_array2.length; i++) {
		System.out.print (test_insert_array2[i] + " "); 
	}
	System.out.println(); 
	}
}