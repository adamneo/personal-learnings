'''
The following code provides a snippet of how to extract data from a set of Salesforce objects and import it into Python. From here, the commands shows how the data can be pushed into a Spark Dataframe within
a Microsoft Azure cloud computing environment. 
'''
# Importing the necessary packages
import os
import ast
import collections
import re
import pandas as pandas
from re import sub
from simple_salesforce import Salesforce 
from datetime import date, timedelta
from azure.storage.blob import BlockBlobService
from pyspark.sql import SQLContect, HiveContext, SparkSession
from pyspark import SparkContext, SparkConf
from pyspark.sql.functions import udf, input_file_name, col, expr, when

# Setting up the SparkContext and HiveContext environments 
try:
	sc 
except NameError:
	sc = SparkContext()
	sqlContext = SQLContext(sc)
	spark = SparkSession(sc)

hc = HiveContext(sc)

# Defining the global variables
current_dt = date.today().strftime('%Y-%m-%d')
current_date = current_dt + 'T00:00:00.000Z'
extract_dt = (date.today() - timedelta(1)).strftime('%Y%m%d')
extract_date = (date.today() - timedelta(1)).strftime('%Y-%m-%d') + 'T00:00:00.000Z'
containter_name = '<insert your container name here>'
schema_name = 'insert your schema name here'

# Funciton to identify and remove line breaks and non ASCII characters. This proved quite problematic with
#  regards to the last Salesforce dataset I worked with, so it will be good to run this regardless in the futurer
def contains_breaks(x):
	if x == None:
		return x
	if type(x) == collections.OrderedDict:
		for i in x.keys():
			if type(x[i]) in (unicode, str):
				x2 = x[i].encode("utf-8").decode("utf-8")
				x2 = x2.replace("\r\n", " ")
				x2 = x2.replace("/n", " ")
				x2 = x2.replace("\n", " ")
				x[i] = re.sub(r'[^\x00-\x7F]+', ' ', x2).encode("ascii").decode("ascii")
		return x
	else:
		x2 = x.encode("utf-8").decode("utf-8")
		x2 = x2.replace("\r\n", " ")
		x2 = x2.replace("/n", " ")
		x2 = x2.replace("\n", " ")
		x[i]  re.sub(r'[^\x00-\x7F]+', ' ', x2).encode("ascii").decode("ascii")	

# Creating a new class structure to process the tables
class schedule:
	def __init__(self, lst):
		self.sf_table = lst[0]
		self.var_set = lst[1]

	def salesforce_tbl(self):
		return(self.sf_table)

	def soql_string(self):
		full_string = self.var_set[0]
		for i in range(1,len(self.var_set)):
			full_string = full_string + ", " + self.var_set[i]
		return full_string

	def variable_set(self):
		return self.var_set

# Defining the Salesforce parameters to estable a connection to the Salesforce API
sf_username = "<insert your username here>"
sf_password = "<insert your password here>"
sf_security = "<insert your security token here>"
sf_sandbox = False # Set to True if you are working with a Dev environment

sf_connect = Salesforce(username = sf_username, password = sf_password, security_token = sf_security, sandbox = sf_sandbox)

# Loading the columns we want to extract from each object in Salesforce. For some bright reason, SOQL queries do not allow us to perform a select *, hence everything will need to be be stated
# if you are planning on extracting out every single column.
sf_load = spark.read.format("csv").option("header","true").option("delimiter", ",").option("inferSchema", "true").option("quote", '"').option("escape", '"').csv("<location>")
sf_objects = sf_load.toPandas()
sf_objects['Field_List'] = sf_objects["Fields"].apply(lambda x: x.split(","))
nrow = len(sf_objects["Object"])
table_listing = []
for i in range(0,nrow,1):
	entry = [sf_objects["Object"][i],sf_objects["Field_List"][i]]
	table_listing.append(entry)

# Changing the data into the bespoke new class.
job_listing = []
for table in table_listing:
	job_listing.append(schedule(table))

# Logging into Azure
az_acct_key = "Azure account key"
az_acct_name = 'Account name'
block_blob_service = BlockBlobService(account_name = az_acct_name, account_key = az_acct_key)

# Loading the list of columns that contains special characters and will need editing.
freetxt_raw = spark.read.format("csv").option("header", "true").option("delimiter", ",").option("inferSchema", "true").option("quote", '"').option("escape", '"').csv("wasb://<foldername>@<storage account name>.blob.core.windows.net/<sub folder name>/<blob name>")
freetxt_frm = freetxt_raw.toPandas()
freetxt_lst = []
for i in range(0,len(freetxt_frm),1):
	free = freetxt_frm["Variable"][i]
	freetxt_lst.append(free)


# Actually running the load
for job in job_listing:
	soql_query = "select {} from {} where (createddate >= {} and createddate <= {}) or (lastmodifieddate >= {} and lastmodifieddate <= {})".format(job.soql_string(), job.salesforce_tbl(), extract_date, current_date, extract_date, current_date)
	extract = sf_connect.query_all(soql_query)

	## Transforming data from an ordered list into a Pandas dataframe
	values = extract["records"]
	list2convert = [dict((i, value[i]) for i in job.variable_set()) for value in values]
	dataset = pd.DataFrame(list2convert)
	dataset["LoadDate"] = current_dt
	field_list = job.variable_set()
	if len(dataset) > 0:
		for field in field_list:
			if field in freetxt_lst:
				dataset[field] = dataset[field].apply(lambda x: contains_breaks(x))
	dataset_extract = dataset.to_csv(index = False, index_label = None, encoding = 'utf-8')
	## Pushing the pandas dataframe into the Azure blob as a csv file.
	blob_name = "<whatever you want the container name to be. Note: you might need to create the container if this is a first time. >"
	## Pushing the data into the blob
	block_blob_service.create_blob_from_text(container_name, blob_name, dataset_extract)

for i in range(0, len(job_listing), 1):
	loc = "wasb://<foldername>@<storage account name>.blob.core.windows.net/<sub folder name>/<blob name>"
	df = spark.read.option("header", "true").option("quote", '"').option("multiLine", "True").option("inferSchema", "True").csv(loc)
	if df.count()>0:
		df.createOrReplaceTempView("pretable")
		query = "insert into table {}.{} select * from pretable".format(schema_name, job_listing[i].salesforce_tbl())
		spark.sql(query)