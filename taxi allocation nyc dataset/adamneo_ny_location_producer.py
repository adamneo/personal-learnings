# Importing the necessary libraries
from kafka import SimpleProducer, KafkaClient
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.sql import Row, SparkSession
import sys
from time import sleep

# Loading the necessary files and defining other elements necesessary for them 
csv_file = "taxi_zone_locations.csv"

# Defining the Kafka settings and setting up the producer
topic = "locations"
kafka = KafkaClient("localhost:9092")
producer = SimpleProducer(kafka)

# Defining a method that will allow us to push each element of the RDD 
def sendDataToKafka(csv_row):
	message = csv_row.encode("utf-8")
	producer.send_messages(topic, message)
	print(message)

# Actual process of sending data to Kafka
print("Running the stream to pass locations")
while True:
	with open(csv_file) as file:
		next(file)
		for line in file:
			line_refined = topic + "," + line.strip("\n")
			sendDataToKafka(line_refined)
print('End of stream')