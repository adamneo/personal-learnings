#!/usr/bin/env python

# Importing the necessary packages
from __future__ import print_function
from pyspark import SparkContext
from pyspark.sql import Row, SparkSession, SQLContext, HiveContext
from pyspark.sql.functions import col, lit, when
from pyspark.sql.types import *
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils

# Method to setup a singleton Hive Context. This method was sourced from Choi (2017).
def getHiveContextInstance(sparkContext):
	if ('sqlContextSingletonInstance' not in globals()):
		globals()['sqlContextSingletonInstance'] = HiveContext(sparkContext)
	return globals()['sqlContextSingletonInstance']


if __name__ == "__main__":

	# Defining the topics we want to consume within our stream
	topic_1 = "locations"
	topic_2 = "nyc_taxi_data"

	# Setting up the Spark and Streaming Contexts
	sc = SparkContext()
	ssc = StreamingContext(sc, 5)
	ssc.checkpoint("checkpoint")
	zkQuorum = "localhost:2181"

	# Setting up the Kafka Stream
	kafka_stream = KafkaUtils.createStream(ssc, zkQuorum, "spark-streaming-consumer1", {topic_1:1, topic_2:1})
	lines = kafka_stream.map(lambda x: (x[1].split(",")))

	# Function to maintain the structure of our location RDD by always returning the latest value within a stateful DStream
	def latestVal(new_value, old_value):
		if len(new_value) != 0:
			return new_value[0]
		else:
			return old_value


	################################################
	### Creating our view on the demand of trips ###
	################################################

	# Here we create a rolling view of trip counts and total fares for each batch. The average fare amount is calculated by dividng total fares by total trips.
	totalTrips = lines.filter(lambda row: row[0]=="nyc_taxi_data").map(lambda row: (row[7], 1)).reduceByKeyAndWindow(lambda x, y: x + y, lambda x, y: x - y, 20, 5)
	totalFares = lines.filter(lambda row: row[0]=="nyc_taxi_data").map( lambda row: (row[7], float(row[18]))).reduceByKeyAndWindow(lambda x, y: x + y, lambda x, y: x - y, 20, 5)

	demand_raw = totalTrips.join(totalFares).map(lambda row: (row[0], row[1][0], row[1][1]/row[1][0]) )

	# Calculating the top trip count and top average fare for each patch
	demand_max_counts = demand_raw.map(lambda row: (1, row[1])).reduceByKey(max)
	demand_max_avg = demand_raw.map(lambda row: (1, row[2])).reduceByKey(max)

	# Returns RDD based on indexes 0: binary indicator, 1: max trip count across batch RDD, 2: Max average fare across RDD
	demand_max = demand_max_counts.join(demand_max_avg).map(lambda row: (row[0], (row[1][0], row[1][1]) ) )

	demand_joiner = demand_raw.map(lambda row: (1, (row[0],row[1],row[2]) ) )

	# The RDD below is an intermediary setup as follows: ( pickup_location, trip_count_for_region, average_fare_for_region, max_trip_count_across_batch, max_average_fare_value_across_batch)
	demand_int = demand_joiner.join(demand_max).map(lambda row: (row[1][0][0], row[1][0][1], row[1][0][2], row[1][1][0], row[1][1][1]) )

	# Creating our final view on demand. The RDD is setup as follows: ( pickup location, ( Scaled Trip Count, Scaled Average Fares) )
	demand = demand_int.map(lambda row: ( row[0], ( row[1]/row[3], row[2]/row[4] ) ) )


	########################################################
	### Creating our view on distances between locations ###
	########################################################

	loc_raw = lines.filter(lambda row: row[0]=="locations").map(lambda row: ((row[1],row[2]), float(row[3]))).updateStateByKey(latestVal)
	# The RDD listed below is setup as follows: (Potential_Location, ( Current_Location, Distance_Score ) )
	locations = loc_raw.map(lambda row: (row[0][0], ( row[0][1], row[1] ) ) )

	###########################################################
	### Creating our view on the supply of available taxis. ###
	###########################################################

	# The supply RDD is setup as follows: ( Drop_Off_Location, Taxi_ID )
	supply = lines.filter(lambda row: row[0]=="nyc_taxi_data").map(lambda row: (row[8], row[1]))


	#####################################
	### Bringing Everything together. ###
	#####################################

	# Joining the locations dataset to the demand dataset
	demand_and_loc = demand.join(locations).map(lambda row: (row[1][1][0], row[0], row[1][0][0], row[1][0][1], row[1][1][1]) )

	# Scoring matches acros our combined demand and location dataset. The scoring system follows the formula described in section 2 of the report. We tune alphas and betas in section 5.
	# The RDD below is setip as follows: (Current_Location, Potential_Location, Scoring_Metric)
	assignment_scores = demand_and_loc.map(lambda row: (row[0], row[1], row[4] * (0.5*row[2] + 0.5*row[3]) ) )

	# Determining the top assigned location for each drop off location. This works similar to a "group by" combined with "rank=1" SQL query, where our group by is done on the keys of the RDD.
	top_rank = assignment_scores.map(lambda row: ( row[0], (row[2], row[1]) ) ).reduceByKey(max)

	# Joining the assignment scores with available taxis
	finalDF = supply.join(top_rank)


	# Method to write rankings into a Hive table. This method was derived from code developed Choi (2017) and modified to fit the requirements of this script
	def writeDF(time, rdd):
		print("========= %s =========" % str(time))
		try:
			hc = getHiveContextInstance(rdd.context)
			newRDD = rdd.map(lambda line: (line[1][0], line[0], line[1][1][1], line[1][1][0]))
			schema = StructType([StructField("taxi_id", StringType(), True), StructField("current_loc", StringType(), True), StructField("assigned_loc", StringType(), True), StructField("score", FloatType(), True)])
			newDF = hc.createDataFrame(newRDD, schema)
			newDF.show(20, False)
			newDF.createOrReplaceTempView("new_table")
			newDF = hc.sql("insert into table stream.test_loads select taxi_id, current_loc, assigned_loc, score from new_table")
		except:
			pass

	#Applying the Hive function across the DStreams
	finalDF.foreachRDD(writeDF)

	# Starting the Streaming session
	ssc.start()
	ssc.awaitTermination()
	