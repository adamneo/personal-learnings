# Importing the necessary libraries
from kafka import SimpleProducer, KafkaClient
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.sql import Row, SparkSession
from time import sleep

# Loading the necessary files and defining other elements necesessary for them 
csv_file_list = ["green_tripdata_2018-12.csv"]

# Defining the Kafka settings and setting up the producer
topic = "nyc_taxi_data"
kafka = KafkaClient("localhost:9092")
producer = SimpleProducer(kafka)

# Defining a method that will allow us to push each element of the RDD 
def sendDataToKafka(csv_row):
	message = csv_row.encode("utf-8")
	producer.send_messages(topic, message)
	print(message)
	#sleep(0.1) # I add in a wait period purely for readability purposes. This can be taken out of the code and everything will still work efficiently. 


# Actual process of sending data to Kafka
if __name__ == "__main__":
	print("Running the stream for taxi data")
	taxi_id = 0 # Here we created an index that increments up by 1 for each trip. This index serves as our unique identifier for each taxi.
	for csv_file in csv_file_list:
		with open(csv_file) as file:
			# We ignore the header and the second line of the file, which is just a line of blanks.
			next(file)
			next(file)
			for line in file:
				line_refined = (topic + "," + str(taxi_id)+ "," +line.strip("\n"))
				sendDataToKafka(line_refined)
				taxi_id +=1 # Increments our taxi id by 1. 
	print('End of stream')